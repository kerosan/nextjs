
## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


design https://www.figma.com/file/ShP1hfnZ2e7hEos0My32jB/Weldes?node-id=0%3A1