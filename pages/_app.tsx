import '../styles/globals.css'
import { FC } from 'react'
import { ThemeProvider } from 'styled-components'
import theme from '../src/components/theme'

const MyApp: FC<{ Component: any; pageProps: any }> = ({
  Component,
  pageProps,
}) => {
  return (
    <ThemeProvider theme={theme}>
      <Component {...pageProps} />
    </ThemeProvider>
  )
}

export default MyApp
