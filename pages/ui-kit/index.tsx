import React, { ReactNode } from 'react'
import { Color, FlexRow } from '../../src/components/UiKit/Color'
import { Button, ButtonSecondary } from '../../src/components/theme'
import {
  H1,
  H2,
  H3,
  H4,
  H5,
  H6,
  Text,
  Link,
  Ul,
  Li,
  Ol,
} from '../../src/components/theme'
import { Tabs, Tab } from '../../src/components/tabs'

import ArrowRight from '../../src/assets/icons/arrow-right.svg'
import ArrowTop from '../../src/assets/icons/arrow-top.svg'
import ArrowBottom from '../../src/assets/icons/arrow-bottom.svg'
import ArrowLeft from '../../src/assets/icons/arrow-left.svg'

import ChevronRight from '../../src/assets/icons/chevron-right.svg'
import ChevronTop from '../../src/assets/icons/chevron-top.svg'
import ChevronLeft from '../../src/assets/icons/chevron-left.svg'
import ChevronBottom from '../../src/assets/icons/chevron-bottom.svg'

import Close from '../../src/assets/icons/close.svg'
import Play from '../../src/assets/icons/play.svg'

import Facebook from '../../src/assets/icons/facebook.svg'
import Vk from '../../src/assets/icons/vk.svg'
import Telegram from '../../src/assets/icons/telegram.svg'
import Instagram from '../../src/assets/icons/instagram.svg'

import { useTheme } from 'styled-components'
import { client } from '../../src/utils/contentfull'

const Index = (): ReactNode => {
  const theme = useTheme()

  client
    // .getContentTypes()
    .getContentType('contacts')
    // getEntry('qmGT1j90PoTQoSOR7XKWZ')
    .then((entry) => console.log(entry.fields))
    .catch((err) => console.log(err))

  return (
    <div style={{ margin: 10 }}>
      <h1>ui kit</h1>
      <Tabs>
        <Tab label={'Typography'}>
          {' '}
          <div>
            <h2>color</h2>
            <FlexRow>
              {Object.keys(theme.palette).map((color) => (
                <Color key={color} value={theme.palette[color]}>
                  {color}
                </Color>
              ))}
            </FlexRow>
            <h2>Type & Text styles</h2>
            <H1>Heading 1</H1>
            <H2>Heading 2</H2>
            <H3>Heading 3</H3>
            <H4>Heading 4</H4>
            <H5>Heading 5</H5>
            <H6>Heading 6</H6>
            <h2>Texts</h2>

            <Text>
              Это основной стиль текста параграфа. Каждый веб-разработчик знает,
              что такое текст-«рыба». Текст этот, несмотря на название, не имеет
              никакого отношения к обитателям водоемов.
            </Text>
            <Text>
              Своим появлением Lorem ipsum обязан древнеримскому философу
              Цицерону, ведь именно из его трактата средневековый книгопечатник
              вырвал отдельные фразы и слова, получив текст-«рыбу».
            </Text>
            <Text>
              Это <b>выделенный текст</b> внутри внутри параграфа.
            </Text>
            <br />
            <Text>
              Вот так выглядит <Link href={'#'}>ссылка в тексте</Link> параграфа
              по умолчанию, вот так ссылка при наведении, это ссылка при
              нажатии, а это посещенная ссылка.
            </Text>
            <br />
            <Text>
              <small>
                Это вспомогательный текст (второстепенный или для описаний).
                Каждый веб-разработчик знает, что такое текст-«рыба». Вот так
                выглядит <Link href={'#'}>ссылка в тексте</Link> параграфа по
                умолчанию, вот так ссылка при наведении, это ссылка при нажатии,
                а это посещенная ссылка.
              </small>
            </Text>
            <h2>Unordered list</h2>
            <Ul>
              <Li>Unordered list item 1</Li>
              <Li>Unordered list item 2</Li>
              <Li>
                Unordered list item
                <br /> two lines
              </Li>
            </Ul>

            <h2>Ordered list</h2>
            <Ol>
              <Li>Ordered list item 1</Li>
              <Li>Ordered list item 2</Li>
            </Ol>
          </div>
        </Tab>
        <Tab label={'Buttons'}>
          <Button>Home</Button>
          <br />
          <Button disabled>Disabled</Button>
          <br />
          <Button>Long name of Button</Button>
          <hr />
          <Button iconOnly={true}>
            <ChevronRight fill="white" />
          </Button>
          <br />
          <Button iconOnly disabled>
            <ChevronRight fill="white" />
          </Button>
          <hr />
          <ButtonSecondary>Home</ButtonSecondary>
          <br />
          <ButtonSecondary disabled>Disabled</ButtonSecondary>
          <br />
          <ButtonSecondary>Long name of Button</ButtonSecondary>
          <hr />
          <ButtonSecondary>
            <ArrowRight fill={theme.palette.orange} /> Home
          </ButtonSecondary>
          <hr />
          <Link big href={'#'}>
            ссылка в тексте
          </Link>
        </Tab>
        <Tab label="icons">
          <ButtonSecondary>
            <ArrowTop />
            Arrow Top
          </ButtonSecondary>
          <ButtonSecondary>
            <ArrowLeft />
            Arrow Left
          </ButtonSecondary>
          <ButtonSecondary>
            <ArrowBottom />
            Arrow Bottom
          </ButtonSecondary>
          <ButtonSecondary>
            <ArrowRight />
            Arrow Right
          </ButtonSecondary>
          <hr />
          <ButtonSecondary>
            <ChevronTop />
            Chevron Top
          </ButtonSecondary>
          <ButtonSecondary>
            <ChevronLeft />
            Chevron Left
          </ButtonSecondary>
          <ButtonSecondary>
            <ChevronBottom />
            Chevron Bottom
          </ButtonSecondary>
          <ButtonSecondary>
            <ChevronRight />
            Chevron Right
          </ButtonSecondary>
          <hr />
          <ButtonSecondary>
            <Close />
            Close
          </ButtonSecondary>
          <Button>
            <Play fill="white" width="32" height="32" />
          </Button>
          <Button>
            <Facebook fill="white" width="32" height="32" />
          </Button>
          <Button>
            <Vk fill="white" width="32" height="32" />
          </Button>
          <Button>
            <Telegram fill="white" width="32" height="32" />
          </Button>
          <Button>
            <Instagram fill="white" width="32" height="32" />
          </Button>
        </Tab>
      </Tabs>
    </div>
  )
}

export default Index
