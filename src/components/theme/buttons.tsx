import styled, { css } from 'styled-components'
import { convertHex2RGBA } from '.'

export const Button = styled.button<{ iconOnly?: boolean }>`
  min-width: 9.6rem;
  height: 3.6rem;

  font-weight: 600;
  font-size: 1.4rem;
  line-height: 1.9rem;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  letter-spacing: 0.03em;
  text-transform: uppercase;

  background: ${({ theme }) => theme.palette.orange};
  color: ${({ theme }) => theme.palette.white};
  border: 1px solid ${({ theme }) => theme.palette.orange};
  border-radius: 3px;
  cursor: pointer;

  ${({ iconOnly }) =>
    iconOnly &&
    css`
      min-width: 4rem;
      height: 4rem;
      border-radius: 0;
      &:hover {
        border-radius: 0;
      }
    `}

  &:hover {
    ${({ iconOnly }) =>
      !iconOnly &&
      css`
        border-radius: 2px;
      `}
    background: ${({ theme }) => convertHex2RGBA(theme.palette.orange, 0.96)};
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
      0px 0px 2px rgba(0, 0, 0, 0.14);
  }

  &:focus {
    outline: ${({ theme }) => theme.palette.orangeDark};
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
      0px 0px 2px rgba(0, 0, 0, 0.14);
  }

  &:active {
    border-radius: 2px;
    background: ${({ theme }) => convertHex2RGBA(theme.palette.orange, 0.96)};
    box-shadow: 0px 1px 10px rgba(0, 0, 0, 0.2), 0px 4px 5px rgba(0, 0, 0, 0.12),
      0px 2px 4px rgba(0, 0, 0, 0.14);
  }

  &:disabled {
    color: ${({ theme }) => theme.palette.secondaryText};
    background-color: ${({ theme }) => theme.palette.disabledText};
    ${({ iconOnly }) =>
      !iconOnly &&
      css`
        border-radius: 2px;
      `}
    border: 1px solid ${({ theme }) => theme.palette.disabledText};
    box-shadow: none;
  }
`

export const ButtonSecondary = styled.button`
  min-width: 9.6rem;
  height: 3.6rem;

  font-weight: 600;
  font-size: 1.4rem;
  line-height: 1.9rem;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  letter-spacing: 0.03em;
  text-transform: uppercase;

  background: ${({ theme }) => theme.palette.transparent};
  color: ${({ theme }) => theme.palette.orange};
  border: 1px solid ${({ theme }) => theme.palette.transparent};
  border-radius: 3px;
  cursor: pointer;

  &:hover {
    border-radius: 2px;
    background: ${({ theme }) => convertHex2RGBA(theme.palette.divider, 0.7)};
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
      0px 0px 2px rgba(0, 0, 0, 0.14);
  }

  &:focus {
    outline: ${({ theme }) => theme.palette.divider};
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.12),
      0px 0px 2px rgba(0, 0, 0, 0.14);
  }

  &:active {
    border-radius: 2px;
    background: ${({ theme }) => convertHex2RGBA(theme.palette.divider, 0.5)};
    box-shadow: 0px 1px 10px rgba(0, 0, 0, 0.2), 0px 4px 5px rgba(0, 0, 0, 0.12),
      0px 2px 4px rgba(0, 0, 0, 0.14);
  }

  &:disabled {
    color: ${({ theme }) => theme.palette.secondaryText};
    background-color: ${({ theme }) => theme.palette.transparent};
    border-radius: 2px;
    border: 1px solid ${({ theme }) => theme.palette.transparent};
    box-shadow: none;
  }

  svg {
    margin-right: 0.8rem;
  }
`
