export * from './typography'
export * from './buttons'

export const theme = {
  palette: {
    white: '#FFFFFF',
    orange: '#FA983E',
    orangeDark: '#EE7738',
    blue: '#1E3C56',
    divider: '#DEDEDE',
    disabledText: '#9E9E9E',
    secondaryText: '#666666',
    primaryText: '#212121',
    error: '#B00020',
    transparent: 'transparent',
  },
}

export { theme as default }

export const convertHex2RGBA = (hexCode: string, opacity?: number): string => {
  let hex = hexCode.replace('#', '')

  if (hex.length === 3) {
    hex = `${hex[0]}${hex[0]}${hex[1]}${hex[1]}${hex[2]}${hex[2]}`
  }

  const r = parseInt(hex.substring(0, 2), 16)
  const g = parseInt(hex.substring(2, 4), 16)
  const b = parseInt(hex.substring(4, 6), 16)

  return opacity ? `rgba(${r},${g},${b},${opacity})` : `rgb(${r},${g},${b}  )`
}
