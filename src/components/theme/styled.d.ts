// import original module declarations
import 'styled-components'
import theme from './index'

type Colors = keyof typeof theme.palette

// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme {
    palette: {
      [key in Colors]: string
    }
  }
}
