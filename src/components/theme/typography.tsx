import styled, { css } from 'styled-components'

export const H1 = styled.h1`
  font-weight: bold;
  font-size: 4.8rem;
  line-height: 150%;

  text-transform: uppercase;

  color: ${({ theme }) => theme.palette.primaryText};
`

export const H2 = styled.h2`
  font-weight: bold;
  font-size: 3.4rem;
  line-height: 150%;

  text-transform: uppercase;

  color: ${({ theme }) => theme.palette.primaryText};
`

export const H3 = styled.h3`
  font-weight: bold;
  font-size: 3.4rem;
  line-height: 141%;

  color: ${({ theme }) => theme.palette.primaryText};
`

export const H4 = styled.h4`
  font-weight: 600;
  font-size: 2.4rem;
  line-height: 133%;

  color: ${({ theme }) => theme.palette.primaryText};
`

export const H5 = styled.h5`
  font-weight: 600;
  font-size: 2rem;
  line-height: 160%;

  color: ${({ theme }) => theme.palette.primaryText};
`

export const H6 = styled.h6`
  font-weight: bold;
  font-size: 1.6rem;
  line-height: 150%;

  color: ${({ theme }) => theme.palette.primaryText};
`

const textCss = css`
  font-weight: normal;
  font-size: 1.6rem;
  line-height: 150%;
`

export const Text = styled.p`
  margin: 0;
  ${textCss};
  color: ${({ theme }) => theme.palette.secondaryText};
  small {
    font-size: 1.4rem;
    line-height: 143%;
    a {
      font-size: 1.4rem;
    }
  }
`

export const Link = styled.a<{ big?: boolean }>`
  ${textCss};
  ${({ big }) =>
    big &&
    css`
      font-size: 2.4rem;
    `}
  color: ${({ theme }) => theme.palette.orange};
  text-decoration: underline;
  text-decoration-style: dashed;

  &:hover {
    text-decoration: underline;
  }
  &:active {
    color: ${({ theme }) => theme.palette.orangeDark};
  }
  &:visited {
    text-decoration: underline;
    color: ${({ theme }) => theme.palette.primaryText};
  }
`

export const Li = styled.li`
  font-weight: normal;
  font-size: 16px;
  line-height: 150%;

  color: ${({ theme }) => theme.palette.secondaryText};
`

export const Ul = styled.ul`
  ${Li} {
    list-style: none;
  }
  ${Li}::before {
    content: '✓';
    color: ${({ theme }) => theme.palette.orange};
    font-weight: bold;
    display: inline-block;
    width: 2rem;
    margin-left: -2rem;
  }
`

export const Ol = styled.ol``
