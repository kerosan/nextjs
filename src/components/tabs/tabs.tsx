import React from 'react'
import { TabItem, TabsContent, TabsHeader } from './layout'

type Props = {}

export const Tabs: React.FC<Props> = (props) => {
  const [activeTab, setTab] = React.useState(0)

  const tabs = React.Children.map(
    props.children,
    (c: React.ReactElement, i) => {
      return { index: i, label: c.props.label, children: c.props.children }
    }
  )

  return (
    <div>
      <TabsHeader>
        {tabs.map((t) => (
          <TabItem
            isActive={activeTab === t.index}
            key={`tab-label-${t.index}-${t.label}`}
            onClick={() => setTab(t.index)}
          >
            {t.label}
          </TabItem>
        ))}
      </TabsHeader>
      <TabsContent>
        {tabs.map(
          (t) =>
            activeTab === t.index && (
              <div key={`tab-content-${t.index}-${t.label}`}>{t.children}</div>
            )
        )}
      </TabsContent>
    </div>
  )
}
