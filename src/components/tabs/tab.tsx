type Props = {
  label?: React.ReactNode
}
export const Tab: React.FC<Props> = (props) => {
  return <div>{props.children}</div>
}
