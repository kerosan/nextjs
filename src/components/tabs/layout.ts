import styled from 'styled-components'

export const TabsHeader = styled.div`
  display: flex;
`
export const TabItem = styled.div<{ isActive: boolean }>`
  font-weight: bold;
  font-size: 1.4rem;
  line-height: 1.9rem;
  display: flex;
  align-items: center;
  text-align: center;
  text-transform: uppercase;
  color: ${({ theme }) => theme.palette.primaryText};

  cursor: pointer;
  margin: 2.5rem 0;
  padding: 0 1.6rem;
  border-left: 1px solid ${({ theme }) => theme.palette.divider};
  &:first-child {
    border-left: none;
  }

  color: ${({ theme, isActive }) =>
    isActive ? theme.palette.orange : theme.palette.primaryText};
  &:hover {
    color: ${({ theme }) => theme.palette.orangeDark};
  }
`

export const TabsContent = styled.div`
  display: flex;
`
