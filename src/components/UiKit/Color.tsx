import styled from 'styled-components'

export const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
`
export const Color = styled.div<{ value: string }>`
  width: 8rem;
  height: 8rem;
  margin: 1rem;
  border-radius: 50%;
  box-shadow: inset 0 0 1rem black;
  background-color: ${(props) => props.value};
  text-shadow: 0 0 1rem white;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-shrink: 0;
`
